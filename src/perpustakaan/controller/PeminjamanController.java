/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package perpustakaan.controller;

import java.util.ArrayList;
import perpustakaan.view.*;
import perpustakaan.model.*;
import perpustakaan.Perpustakaan;
import perpustakaan.controller.helper.PeminjamanManager;

/**
 *
 * @author tuary
 */
public class PeminjamanController {
     ArrayList<BukuDipinjam> listCollection = Peminjaman.getBukuDipinjam();
     public void showFormPeminjaman() {
        Perpustakaan.formPeminjaman = new FormPeminjaman();
        Perpustakaan.formPeminjaman.tampilkan(); 
    }
    public void cariBuku(String judul){
         BukuProvider bukuProvider = new BukuProvider();
        try {
            ArrayList<Buku> listBuku = bukuProvider.selectBuku(judul);
            if(listBuku.isEmpty())
            {
                DialogUI dialogUI = new DialogUI("Buku tidak terdaftar");
                dialogUI.pack();
                dialogUI.setLocationRelativeTo(null);
                dialogUI.setVisible(true);
            } 
            else Perpustakaan.formPeminjaman.display(listBuku);
        } catch(Exception ex) {
            DialogUI dialogUI = new DialogUI("Connection Error");
            dialogUI.pack();
            dialogUI.setLocationRelativeTo(null);
            dialogUI.setVisible(true);
        }
    }
    public void pinjam(BukuDipinjam buku){
        Perpustakaan.peminjamanManager = new PeminjamanManager();
        this.listCollection.add(buku);
        boolean isSaved = Perpustakaan.peminjamanManager.save(this.listCollection);
        if(isSaved){
            Perpustakaan.formPeminjaman.tampilPinjaman(Peminjaman.getBukuDipinjam());
        }
    }
    public boolean validatelamaPeminjaman(int lama){
        return lama <= 3 && lama > 0;
    }
    public boolean validateBanyakBukuDipinjam(ArrayList <BukuDipinjam> bukuList){
        return bukuList.size() <= 9;
    }
}
