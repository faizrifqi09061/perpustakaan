/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package perpustakaan.model;

import java.util.ArrayList;

/**
 *
 * @author tuary
 */
public class Peminjaman {
    private static ArrayList<BukuDipinjam> daftarBuku = new ArrayList<>();

    public Peminjaman(ArrayList<BukuDipinjam> daftarBuku) {
        Peminjaman.daftarBuku = daftarBuku;
    }
    
    public static ArrayList<BukuDipinjam> getBukuDipinjam(){
        return Peminjaman.daftarBuku;
    }
    
    public static void removeBukuDipinjam(int index){
        Peminjaman.daftarBuku.remove(index);
    }
    
    
    
}   
